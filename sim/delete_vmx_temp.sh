#!/bin/bash
virsh vol-delete --pool default sim1-border01-vmx-vcp-hdb-18.2R1.9-base.qcow2
virsh vol-delete --pool default sim1-border02-vmx-vcp-hdb-18.2R1.9-base.qcow2
virsh vol-delete --pool default sim1-border01-vmx-vcp-hdc-18.2R1.9-base.img
virsh vol-delete --pool default sim1-border02-vmx-vcp-hdc-18.2R1.9-base.img