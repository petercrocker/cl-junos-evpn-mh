# -*- mode: ruby -*-
# vi: set ft=ruby :

# Created: 2019-08-31--16-34-00

def get_mac(oui="28:b7:ad")
  "Generate a MAC address"
  nic = (1..3).map{"%0.2x"%rand(256)}.join(":")
  return "#{oui}:#{nic}"
end

cwd = Dir.pwd.split("/").last
username = ENV['USER']
domain_prefix = "#{username}_#{cwd}"
domain_uuid = "7337619b-ec2c-56c3-a21f-cc95bd57ddf8"

Vagrant.require_version ">= 2.1.0"
Vagrant.configure("2") do |config|

  config.vm.define "r1" do |node|
    guest_name = "r1"
    node.vm.box = "juniper/vmx-vcp"
    node.vm.box_version = "19.1R1.6"
    node.vm.guest = :tinycore
    node.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    node.ssh.insert_key = false

    node.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{domain_prefix}"
      domain.cpus = 1
      domain.memory = 1024
      domain.disk_bus = "ide"
      domain.nic_adapter_count = 1
      domain.storage :file, :path => "#{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2", :size => "99155968", :type => "qcow2", :bus => "ide", :device => "hdb", :allow_existing => true
      domain.storage :file, :path => "#{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img", :size => "10485760", :type => "raw", :bus => "ide", :device => "hdc", :allow_existing => true
    end

    add_volumes = [
      "virsh vol-create-as default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2 99155968",
      "sleep 1",
      "virsh vol-upload --pool default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2 /opt/vagrant/storage/vmx-vcp-hdb-19.1R1.6-base.qcow2",
      "sleep 1",
      "virsh vol-create-as default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img 10485760",
      "sleep 1",
      "virsh vol-upload --pool default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img /opt/vagrant/storage/vmx-vcp-hdc-19.1R1.6-base.img",
      "sleep 1"
    ]
    add_volumes.each do |i|
      node.trigger.before :up do |trigger|
        trigger.name = "add-volumes"
        trigger.info = "Adding Volumes"
        trigger.run = {inline: i}
      end
    end

    delete_volumes = [
      "virsh vol-delete #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2 default",
      "virsh vol-delete #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img default"
    ]
    delete_volumes.each do |i|
      node.trigger.after :destroy do |trigger|
        trigger.name = "remove-volumes"
        trigger.info = "Removing Volumes"
        trigger.run = {inline: i}
      end
    end

    node.vm.network :private_network,
      # r1-internal-1 <--> r1-vfp-internal-1
      :mac => "#{get_mac()}",
      :libvirt__tunnel_type => "udp",
      :libvirt__tunnel_local_ip => "127.69.69.1",
      :libvirt__tunnel_local_port => 11001,
      :libvirt__tunnel_ip => "127.69.69.2",
      :libvirt__tunnel_port => 11001,
      :libvirt__iface_name => "r1-internal-1-#{domain_uuid}",
      auto_config: false

  end
  config.vm.define "r1-vfp" do |node|
    guest_name = "r1-vfp"
    node.vm.box = "juniper/vmx-vfp"
    node.vm.box_version = "19.1R1.6"
    node.vm.guest = :tinycore
    node.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    node.ssh.insert_key = false
    node.ssh.username = "root"

    node.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{domain_prefix}"
      domain.cpus = 3
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.nic_adapter_count = 11
    end


    node.vm.network :private_network,
      # r1-vfp-internal-1 <--> r1-internal-1
      :mac => "#{get_mac()}",
      :libvirt__tunnel_type => "udp",
      :libvirt__tunnel_local_ip => "127.69.69.2",
      :libvirt__tunnel_local_port => 11001,
      :libvirt__tunnel_ip => "127.69.69.1",
      :libvirt__tunnel_port => 11001,
      :libvirt__iface_name => "r1-vfp-internal-1-#{domain_uuid}",
      auto_config: false

    node.vm.network :private_network,
      # r1-vfp-ge-0/0/0 <--> r2-vfp-ge-0/0/0
      :mac => "#{get_mac()}",
      :libvirt__tunnel_type => "udp",
      :libvirt__tunnel_local_ip => "127.69.69.2",
      :libvirt__tunnel_local_port => 10000,
      :libvirt__tunnel_ip => "127.69.69.4",
      :libvirt__tunnel_port => 10000,
      :libvirt__iface_name => "r1-vfp-ge-0/0/0-#{domain_uuid}",
      auto_config: false

  end
  config.vm.define "r2" do |node|
    guest_name = "r2"
    node.vm.box = "juniper/vmx-vcp"
    node.vm.box_version = "19.1R1.6"
    node.vm.guest = :tinycore
    node.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    node.ssh.insert_key = false

    node.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{domain_prefix}"
      domain.cpus = 1
      domain.memory = 1024
      domain.disk_bus = "ide"
      domain.nic_adapter_count = 1
      domain.storage :file, :path => "#{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2", :size => "99155968", :type => "qcow2", :bus => "ide", :device => "hdb", :allow_existing => true
      domain.storage :file, :path => "#{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img", :size => "10485760", :type => "raw", :bus => "ide", :device => "hdc", :allow_existing => true
    end

    add_volumes = [
      "virsh vol-create-as default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2 99155968",
      "sleep 1",
      "virsh vol-upload --pool default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2 /opt/vagrant/storage/vmx-vcp-hdb-19.1R1.6-base.qcow2",
      "sleep 1",
      "virsh vol-create-as default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img 10485760",
      "sleep 1",
      "virsh vol-upload --pool default #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img /opt/vagrant/storage/vmx-vcp-hdc-19.1R1.6-base.img",
      "sleep 1"
    ]
    add_volumes.each do |i|
      node.trigger.before :up do |trigger|
        trigger.name = "add-volumes"
        trigger.info = "Adding Volumes"
        trigger.run = {inline: i}
      end
    end

    delete_volumes = [
      "virsh vol-delete #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdb-19.1R1.6-base.qcow2 default",
      "virsh vol-delete #{domain_prefix}-#{guest_name}-#{domain_uuid}-vmx-vcp-hdc-19.1R1.6-base.img default"
    ]
    delete_volumes.each do |i|
      node.trigger.after :destroy do |trigger|
        trigger.name = "remove-volumes"
        trigger.info = "Removing Volumes"
        trigger.run = {inline: i}
      end
    end

    node.vm.network :private_network,
      # r2-internal-1 <--> r2-vfp-internal-1
      :mac => "#{get_mac()}",
      :libvirt__tunnel_type => "udp",
      :libvirt__tunnel_local_ip => "127.69.69.3",
      :libvirt__tunnel_local_port => 11001,
      :libvirt__tunnel_ip => "127.69.69.4",
      :libvirt__tunnel_port => 11001,
      :libvirt__iface_name => "r2-internal-1-#{domain_uuid}",
      auto_config: false

  end
  config.vm.define "r2-vfp" do |node|
    guest_name = "r2-vfp"
    node.vm.box = "juniper/vmx-vfp"
    node.vm.box_version = "19.1R1.6"
    node.vm.guest = :tinycore
    node.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    node.ssh.insert_key = false
    node.ssh.username = "root"

    node.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{domain_prefix}"
      domain.cpus = 3
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.nic_adapter_count = 11
    end

    node.vm.network :private_network,
      # r2-vfp-internal-1 <--> r2-internal-1
      :mac => "#{get_mac()}",
      :libvirt__tunnel_type => "udp",
      :libvirt__tunnel_local_ip => "127.69.69.4",
      :libvirt__tunnel_local_port => 11001,
      :libvirt__tunnel_ip => "127.69.69.3",
      :libvirt__tunnel_port => 11001,
      :libvirt__iface_name => "r2-vfp-internal-1-#{domain_uuid}",
      auto_config: false

    node.vm.network :private_network,
      # r2-vfp-ge-0/0/0 <--> r1-vfp-ge-0/0/0
      :mac => "#{get_mac()}",
      :libvirt__tunnel_type => "udp",
      :libvirt__tunnel_local_ip => "127.69.69.4",
      :libvirt__tunnel_local_port => 10000,
      :libvirt__tunnel_ip => "127.69.69.2",
      :libvirt__tunnel_port => 10000,
      :libvirt__iface_name => "r2-vfp-ge-0/0/0-#{domain_uuid}",
      auto_config: false

  end

end