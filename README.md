# cl-junos-evpn-mh

This repo is for EVPN multihoming interoperability between Cumulus VX version 4.2.0 and JunOS vMX 18.2R1.9 (trial version).

## Setup

Create a vagrant libvirt box of Juniper vMX: https://codingpackets.com/blog/juniper-vmx-19-1r1-6-vagrant-libvirt-box-install/
NOTE: There's one additional config that needs to be applied when creating the vagrant box:
`set int ge-0/0/0 unit 0 family inet dhcp`

Once the box is installed, the following repo should work with no errors.

```
cd cl-junos-evpn-mh/sim
vagrant up oob-mgmt-server oob-mgmt-switch
vagrant up
vagrant ssh oob-mgmt-server
```

Then on OOB server:
```
git clone https://gitlab.com/petercrocker/cl-junos-evpn-mh
cd cl-junos-evpn-mh/automation
ansible-playbook config_restore.yml
```